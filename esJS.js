let classifica = [
    { id: 1, teamName: "Arsenal", P: 38, W: 26, L: 9, D: 3, F: 79, A: 36, pts: 87 },
    { id: 2, teamName: "Liverpool", P: 38, W: 24, L: 8, D: 6, F: 67, A: 30, pts: 80 },
    { id: 3, teamName: "Manchester_U", P: 38, W: 24, L: 5, D: 9, F: 87, A: 45, pts: 77 },
    { id: 4, teamName: "Newcastle", P: 38, W: 21, L: 8, D: 9, F: 74, A: 52, pts: 71 },
    { id: 5, teamName: "Leeds", P: 38, W: 18, L: 12, D: 8, F: 53, A: 37, pts: 66 },
    { id: 6, teamName: "Chelsea", P: 38, W: 17, L: 13, D: 8, F: 66, A: 38, pts: 64 },
    { id: 7, teamName: "West_Ham", P: 38, W: 15, L: 8, D: 15, F: 48, A: 57, pts: 53 },
    { id: 8, teamName: "Aston_Villa", P: 38, W: 12, L: 14, D: 12, F: 46, A: 47, pts: 50 },
    { id: 9, teamName: "Tottenham", P: 38, W: 14, L: 8, D: 16, F: 49, A: 53, pts: 50 },
    { id: 10, teamName: "Blackburn", P: 38, W: 12, L: 10, D: 16, F: 55, A: 51, pts: 46 },
    { id: 11, teamName: "Southampton", P: 38, W: 12, L: 9, D: 17, F: 46, A: 54, pts: 45 },
    { id: 12, teamName: "Middlesbrough", P: 38, W: 12, L: 9, D: 17, F: 35, A: 47, pts: 45 },
    { id: 13, teamName: "Fulham", P: 38, F: 36, W: 10, L: 14, D: 14, A: 44, pts: 44 },
    { id: 14, teamName: "Charlton", P: 38, W: 10, L: 14, D: 14, F: 38, A: 49, pts: 44 },
    { id: 15, teamName: "Everton", P: 38, W: 11, L: 10, D: 17, F: 45, A: 57, pts: 43 },
    { id: 16, teamName: "Bolton", P: 38, W: 9, L: 13, D: 16, F: 44, A: 62, pts: 40 },
    { id: 17, teamName: "Sunderland", P: 38, W: 10, L: 10, D: 18, F: 29, A: 51, pts: 40 },
    { id: 18, teamName: "Ipswich", P: 38, W: 9, L: 9, D: 20, F: 41, A: 64, pts: 36 },
    { id: 19, teamName: "Derby", P: 38, W: 8, L: 6, D: 24, F: 33, A: 63, pts: 30 },
    { id: 20, teamName: "Leicester", P: 38, W: 5, L: 13, D: 20, F: 30, A: 64, pts: 28 }
]


/* DEFINISCO LE DIFFERENZe RETI E Le METTO IN UN ARRAY */
const arr = []
for (let i = 0; i < classifica.length; i++) {

    document.getElementById("tabella").innerHTML += `
        <tr>
        <td>${classifica[i].id}</td>
        <td>${classifica[i].teamName}</td>
        <td>${classifica[i].pts}</td>
        <td>${classifica[i].W}</td>
        <td>${classifica[i].D}</td>
        <td>${classifica[i].L}</td>
        <td>${classifica[i].F}</td>
        <td>${classifica[i].A}</td>
        </tr>
    ` 
    const diffReti = (classifica[i].F - classifica[i].A);
    arr.push(Math.abs(diffReti))

    function minNum(arr) {
        var min = Math.min.apply(null, arr);
        let ind = arr.indexOf(min);
        console.log(classifica[ind].teamName)
        document.getElementById("frase").innerHTML = '<p>La squadra con la minor differenza reti è: '+'<b>' + classifica[ind].teamName +'</b></p>';
    }
}
console.log(arr)
minNum(arr);




