const mese = [

    { giorno: 1, maxTemp: 88, minTemp: 59 },
    { giorno: 2, maxTemp: 79, minTemp: 63 },
    { giorno: 3, maxTemp: 77, minTemp: 55 },
    { giorno: 4, maxTemp: 77, minTemp: 59 },
    { giorno: 5, maxTemp: 90, minTemp: 66 },
    { giorno: 6, maxTemp: 81, minTemp: 61 },
    { giorno: 7, maxTemp: 73, minTemp: 57 },
    { giorno: 8, maxTemp: 75, minTemp: 54 },
    { giorno: 9, maxTemp: 86, minTemp: 32 },
    { giorno: 10, maxTemp: 84, minTemp: 64 },
    { giorno: 11, maxTemp: 91, minTemp: 59 },
    { giorno: 12, maxTemp: 88, minTemp: 73 },
    { giorno: 13, maxTemp: 70, minTemp: 59 },
    { giorno: 14, maxTemp: 61, minTemp: 59 },
    { giorno: 15, maxTemp: 64, minTemp: 55 },
    { giorno: 16, maxTemp: 79, minTemp: 59 },
    { giorno: 17, maxTemp: 81, minTemp: 57 },
    { giorno: 18, maxTemp: 82, minTemp: 52 },
    { giorno: 19, maxTemp: 81, minTemp: 61 },
    { giorno: 20, maxTemp: 84, minTemp: 57 },
    { giorno: 21, maxTemp: 86, minTemp: 59 },
    { giorno: 22, maxTemp: 90, minTemp: 64 },
    { giorno: 23, maxTemp: 90, minTemp: 68 },
    { giorno: 24, maxTemp: 90, minTemp: 77 },
    { giorno: 25, maxTemp: 90, minTemp: 72 },
    { giorno: 26, maxTemp: 97, minTemp: 64 },
    { giorno: 27, maxTemp: 91, minTemp: 72 },
    { giorno: 28, maxTemp: 84, minTemp: 68 },
    { giorno: 29, maxTemp: 88, minTemp: 66 },
    { giorno: 30, maxTemp: 90, minTemp: 45 },
    { giorno: 31, maxTemp: 82.9, minTemp: 60.5 }
]

const arr = [];

for (let i = 0; i< mese.length; i++){

    document.getElementById("tabella").innerHTML += `
    <tr>
    <td>${mese[i].giorno}</td>
    <td>${mese[i].maxTemp}</td>
    <td>${mese[i].minTemp}</td>
    </tr>
    `
    const diffTemp = (mese[i].maxTemp - mese[i].minTemp);
    arr.push(diffTemp)
    console.log(diffTemp)

    function minTemp(arr){
        let min = Math.min.apply(null, arr);
        let ind = arr.indexOf(min);
        console.log(mese[ind].giorno)
        document.getElementById("frase").innerHTML = "<p>Il giorno con l'escursione termica pių piccola è stato il " + "<b>"+mese[ind].giorno +"</b> del mese</p>"
    }
}
minTemp(arr)